package drawing.version2;

import shapes.Circle;
import shapes.Rectangle;

public class TestDrawing {

	public static void main(String[] args) {
		
		Drawing drawing = new Drawing();
		
		drawing.addShape(new Circle(5));
		drawing.addShape(new Rectangle(5,6));
		

		System.out.println("Total area = " + drawing.calculateTotalArea());
	}

}
