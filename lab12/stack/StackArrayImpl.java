package stack;

import java.util.ArrayList;

public class StackArrayImpl implements Stack{

    ArrayList stack = new ArrayList();

    public void push(Object item){
        stack.add(item);
    }

    public Object pop(){
        if (!empty()) {
            return stack.remove(stack.size() - 1);
        }
        else
            return null;
    }

    public boolean empty(){
        return stack.size() == 0;
    }
}
