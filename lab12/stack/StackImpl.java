package stack;

import java.util.ArrayList;

public class StackImpl implements Stack{

    StackItem top;

    public ArrayList<StackItem> StackArray = new ArrayList<StackItem>();

    public Object pop(){
        Object popItem = top.getItem();
        top = top.getNext();
        return popItem;
    }

    public void push(Object item){
        StackItem stackItem = new StackItem(item);
        stackItem.setNext(top);
        top = stackItem;
    }

    public boolean empty(){
        return top == null;
    }
}
