package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Board board = new Board();
        int row;
        int col;
        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();

            try
            {
                System.out.print("Player " + player + " enter row number:");
                row = Integer.valueOf(reader.nextLine());
            }catch (NumberFormatException e) {
                System.out.println("Invalid input.");
                continue;
            }
            try
            {
                System.out.print("Player " + player + " enter column number:");
                col = Integer.valueOf(reader.nextLine());
            }catch (NumberFormatException e){
                System.out.println("Invalid input.");
                continue;
            }
            try {
                board.move(row, col);
            }catch (InvalidMoveException s){
                System.out.println(s.getMessage());
            }
            System.out.println(board);
        }


        reader.close();
    }


}
