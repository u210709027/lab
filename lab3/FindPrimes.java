public class FindPrimes {
    public static void main(String[] args) {
        int number = Integer.parseInt(args[0]);
        boolean check;
        int count = 1;
        for (int i = 2; i <= number; i++){
            check = true;
            for (int j = 2; j < i; j++){
                if (i % j == 0){
                    check = false;
                    break;
                }

            }
            if (check && count == 1)
                System.out.print(i);
            else if (check)
                System.out.print("," + i);
            count++;
        }
    }
}
