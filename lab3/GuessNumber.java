import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		int guess;
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		guess = reader.nextInt();
		int count = 1;
		while (guess != number && guess != -1){
			count++;
			System.out.println("Sorry!");
			String compare = guess > number ? "Mine is less than your guess" : "Mine is greater than your guess";
			System.out.println(compare);
			System.out.print("Type -1 to quit or guess another: ");
			guess = reader.nextInt();
		}
		if (guess == number)
			System.out.println("Congratulations! You won after " + count + " attempts!");
		else
			System.out.println("Sorry, the number was " + number);

		
		
		reader.close(); //Close the resource before exiting
	}
}