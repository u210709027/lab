import java.util.Scanner;

public class tictactest {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        char[][] board = {{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}};
        int row;
        int col;
        int turn = 0;
        int player;
        printBoard(board);
        while (true) {
            player = getTurn(turn);
            System.out.print("Player " + player + " enter row number: ");
            row = reader.nextInt() - 1;
            System.out.print("Player " + player + " enter column number: ");
            col = reader.nextInt() - 1;
            if (row > 2 || col > 2 || row < 0 || col < 0) {
                System.out.println("\nInvalid input.\n");
                continue;
            }
            if (board[row][col] != 'X' && board[row][col] != 'O') {
                if (player == 1)
                    board[row][col] = 'X';
                else
                    board[row][col] = 'O';
            }
            else{
                System.out.println("\nTile is already occupied. Choose another one.\n");
                continue;
            }
            printBoard(board);
            if (checkBoard(board)) {
                System.out.println("Player " + player + " wins!");
                break;
            }
            turn++;
            if (turn == 9){
                System.out.println("Draw!");
                break;
            }
        }

    }
    public static void printBoard(char[][] board){
        System.out.println("    1   2   3");
        System.out.println("  _____________");
        for (int row = 0; row < 3; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; col++) {
                System.out.print("| " + board[row][col] + " ");
            }
            System.out.println("|");
            System.out.println("  -------------");
        }
    }
    public static int getTurn(int turn){
        if (turn % 2 == 1)
            return 2;
        return 1;
    }
    public static boolean checkBoard(char[][] board){
        int row_x;
        int row_o;
        int col_x;
        int col_o;
        boolean game_over = false;
        for (int i = 0; i < 3; i++) {
            row_x = 0;
            row_o = 0;
            col_x = 0;
            col_o = 0;
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == 'X')
                    row_x++;
                if (board[i][j] == 'O')
                    row_o++;
                if (board[j][i] == 'X')
                    col_x++;
                if (board[j][i] == 'O')
                    col_o++;
            }
            if (row_x == 3 || col_x == 3)
                game_over = true;
            else if (row_o == 3 || col_o == 3)
                game_over = true;
            else if ((board[0][0] == 'X' && board[1][1] == 'X' && board[2][2] == 'X')
                    || (board[0][2] == 'X' && board[1][1] == 'X' && board[2][0] == 'X'))
                game_over = true;
            else if ((board[0][0] == 'O' && board[1][1] == 'O' && board[2][2] == 'O')
                    || (board[0][2] == 'O' && board[1][1] == 'O' && board[2][0] == 'O'))
                game_over = true;
        }
        return game_over;
    }
}