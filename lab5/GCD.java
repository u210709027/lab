public class GCD{
	public static void main(String[] args){
		int n1 = Integer.parseInt(args[0]);
		int n2 = Integer.parseInt(args[1]);
		System.out.println(GCDRecursive(n1, n2));
	}

	public static int GCDLoop(int n1, int n2){
		int gcd = 1;
		int lesser = n1 > n2 ? n2 : n1;
		for (int i = 1; i <= lesser; i++){
			if (n1 % i == 0 && n2 % i == 0)
				gcd = i;
		}
		
		return gcd;
	}

	public static int GCDRecursive(int n1, int n2){
		int a;
		if (n1 == 1 || n2 == 1)
			return 1;
		if (n1 % n2 == 0 || n2 % n1 == 0)
			return n1 < n2 ? n1 : n2;

		return GCDRecursive(n2, n1 % n2);
	}
}