public class Circle {
    int radius;
    Point center;
    static final double PI = 3.14;

    public Circle(Point center, int raidus){
        this.radius = raidus;
        this.center = center;
    }

    public double area(){
        return (radius * radius) * PI;
    }

    public double perimeter(){
        return 2 * PI * radius;
    }

    public boolean intersect(Circle circle2){
        boolean intersect = false;

        return center.distanceFromPoint(circle2.center) <= radius + circle2.radius;
    }
}
