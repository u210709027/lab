public class Main {
    public static void main(String[] args) {
        Rectangle r = new Rectangle(new Point(3, 2), 10, 12);

        System.out.println("Area of Rectangle: " + r.area());
        System.out.println("Perimeter of Rectangle: " + r.perimeter());

        Point[] corners = r.corners();
        for (int i = 0; i < corners.length; i++) {
            Point p = corners[i];
            if (p == null)
                System.out.println("p is null");
            else
                System.out.println("Corner " + (i+1) + " x = " + p.getxCoord() + " y = " + p.getyCoord());
        }
        Point p1 = new Point(10, 10);
        Point p2 = new Point(20, 20);
        Circle c1 = new Circle(p1, 5);
        Circle c2 = new Circle(p2, 10);

        System.out.println("Circle 1 area: " + c1.area());
        System.out.println("Circle 1 perimeter: " + c1.perimeter());

        System.out.println(c1.intersect(c2));
    }
}
