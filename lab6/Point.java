public class Point {
    private int xCoord;
    private int yCoord;

    public Point(int xCoord, int yCoord){
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    public int getxCoord() {
        return xCoord;
    }

    public int getyCoord() {
        return yCoord;
    }

    public double distanceFromPoint(Point p2){
        int xDiff = xCoord - p2.xCoord;
        int yDiff = yCoord - p2.yCoord;

        return Math.sqrt((xDiff * xDiff) + (yDiff * yDiff));
    }
}
