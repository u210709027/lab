public class Rectangle {
    private int width;
    private int height;
    private Point topLeft;

    public Rectangle(Point topLeft, int width, int height){
        this.width = width;
        this.height = height;
        this.topLeft = topLeft;
    }

    public int area(){
        return width * height;
    }

    public int perimeter(){
        return (2 * width) + (2 * height);
    }

    public Point[] corners(){
        Point topRight = new Point(topLeft.getxCoord() + width, topLeft.getyCoord());
        Point bottomLeft = new Point(topLeft.getxCoord(), topLeft.getyCoord() - height);
        Point bottomRight = new Point(topRight.getxCoord(), topRight.getyCoord() - height);

        Point[] cornerArray = {topLeft, topRight, bottomLeft, bottomRight};

        return cornerArray;
    }
}
