public class MyDate {
    public int day;
    public int month;
    public int year;
    int[] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31};

    public MyDate(int day, int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String toString(){
        return year + "-" + (month < 10 ? "0" : "") + month + "-" + (day < 10 ? "0" : "") + day;
    }

    public boolean inLeapYear(){
        return (year % 4) == 0;
    }

    public void incrementDay(){
        day++;
        if (day > maxDays[month-1]){
            day = 1;
            month++;
            if (month > 12){
                month = 1;
                year++;
            }
        }else if (month == 2 && day == 29 && !inLeapYear()){
            day = 1;
            month++;
        }
    }

    public void incrementMonth(){
        incrementMonth(1);
    }

    public void incrementYear(){
        incrementYear(1);
    }

    public void incrementYear(int diff){
        year += diff;
        if (month == 2 && day > 28 && !inLeapYear()){
            day = 28;
        }
    }

    public void decrementDay(){
        day--;
        if (day < 1){
            month--;
            day = (!inLeapYear() && month == 2) ? maxDays[month-1] - 1 : maxDays[month-1];

        }
    }

    public void decrementYear(){
        incrementYear(-1);
    }

    public void decrementMonth(){
        month--;
        if (month < 1){
            month = (month % 12) + 12;
            year--;
        }
        if (maxDays[month - 1] < day)
            day = maxDays[month - 1];

        if (month == 2 && day > 28 && !inLeapYear()){
            day = 28;
        }
    }

    public void incrementDay(int diff){
        while (diff > 0){
            incrementDay();
            diff--;
        }
    }

    public void incrementMonth(int diff){
        month += diff;
        year = month > 12 ? year + (month / 12) : year;
        month = month != 12 ? month % 12 : month;
        day = day > maxDays[month - 1] ? maxDays[month - 1] : day;
        if (month == 2 && day > 28 && !inLeapYear()){
            day = 28;
        }
    }

    public void decrementMonth(int diff){
        while (diff > 0){
            decrementMonth();
            diff--;
        }
    }

    public void decrementDay(int diff){
        while (diff > 0){
            decrementDay();
            diff--;
        }
    }

    public void decrementYear(int diff){
        year -= diff;
        if (month == 2 && day > 28 && !inLeapYear()){
            day = 28;
        }
    }

    public boolean isBefore(MyDate date2){
        int date1length = year * 365 + (month - 1) * 30 + day;
        int date2length = date2.year * 365 + (date2.month - 1) * 30 + date2.day;
        if (year == date2.year && month != date2.month)
            return month < date2.month;
        return date1length < date2length;
    }

    public boolean isAfter(MyDate date2){
        return !isBefore(date2);
    }

    public int dayDifference(MyDate date2){
        int date1MonthToDay = 0;
        for (int i = 0; i < month - 1; i++) {
            date1MonthToDay += maxDays[i];
        }
        if (month > 2 && !inLeapYear())
            date1MonthToDay--;
        int date1Day = year * 365 + date1MonthToDay + day;

        int date2MonthToDay = 0;
        for (int j = 0; j < date2.month - 1; j++) {
            date2MonthToDay += maxDays[date2.month];
        }
        if (date2.month > 2 && !inLeapYear())
            date2MonthToDay--;
        int date2Day = date2.year * 365 + date2MonthToDay + date2.day;

        return Math.abs(date2Day - date1Day);
    }
}
