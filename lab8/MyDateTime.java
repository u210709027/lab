public class MyDateTime {

    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time){
        this.date = date;
        this.time = time;
    }

    public String toString(){
        return date + " " + time;
    }

    public void incrementDay(){
        date.incrementDay();
    }

    public void incrementHour(){
        incrementHour(1);
    }

    public void incrementHour(int diff){
        int dayDiff = time.incrementHour(diff);
        if (dayDiff < 0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);
    }

    public void decrementHour(int diff){
        incrementHour(-diff);
    }

    public void incrementMinute(int diff){
        int dayDiff = time.incrementMinute(diff);
        if (dayDiff < 0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);
    }

    public void decrementMinute(int diff){
        incrementMinute(-diff);
    }


    public void incrementDay(int diff){
        date.incrementDay(diff);
    }

    public void incrementYear(int diff){
        date.incrementYear(diff);
    }

    public void decrementDay(){
        date.decrementDay();
    }

    public void decrementDay(int diff){
        date.decrementDay(diff);
    }

    public void decrementYear(){
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void decrementMonth(int diff){
        date.decrementMonth(diff);
    }

    public void incrementMonth(int diff){
        date.incrementMonth(diff);
    }

    public void decrementYear(int diff){
        date.decrementYear(diff);
    }

    public void incrementMonth(){
        date.incrementMonth();
    }

    public void incrementYear(){
        date.incrementYear();
    }



//    public String hourDiff(MyDateTime date2){
//        if (!time.isAfterHour(date2.time)){
//
//        }
//    }

    public boolean isBefore(MyDateTime date2){
        if (date.toString().replaceAll("-", "").equals(date2.date.toString().replaceAll("-", ""))){
            return !time.isAfterHour(date2.time) && !time.isEqualHour(date2.time);
        }
        return date.isBefore(date2.date);
    }

    public boolean isAfter(MyDateTime date2){
        return !isBefore(date2);
    }

    public String dayTimeDifference(MyDateTime date2){
        int minDiff = 0;
        int hourDiff;
        int dayDiff;
        if (isBefore(date2)){
            MyDateTime date1copy = new MyDateTime(new MyDate(date.day, date.month+1, date.year), new MyTime(time.hour, time.minute));
            while (date1copy.isBefore(date2)){
                date1copy.incrementMinute(1);
                minDiff++;
            }
        } else if (isAfter(date2)) {
            MyDateTime date2copy = new MyDateTime(new MyDate(date2.date.day, date2.date.month+1, date2.date.year), new MyTime(date2.time.hour, date2.time.minute));
            while (date2copy.isBefore(this)) {
                date2copy.incrementMinute(1);
                minDiff++;
            }
        }
        hourDiff = minDiff / 60;
        minDiff = minDiff % 60;
        dayDiff = hourDiff / 24;
        hourDiff = hourDiff % 24;

        return (dayDiff == 0 ? "" : dayDiff + " day(s) ") + (hourDiff == 0 ? "" : hourDiff + " hour(s) ")
                + (minDiff == 0 ? "" : minDiff + " minute(s)");

    }

//    public String dayTimeDifference(MyDateTime date2){
//        //MyDateTime date1copy = new MyDateTime(date, time);
//        MyDateTime date2copy = new MyDateTime(date2.date, date2.time);
//        //date1copy.decrementDay();
//        date2copy.decrementDay();
//        if (date.toString().equals(date2copy.date.toString()) )
//    }
}
