public class MyTime {
    int hour;
    int minute;

    public MyTime(int hour, int minute){
        this.hour = hour;
        this.minute = minute;
    }

    public String toString(){
        return (hour < 10 ? "0" : "") + hour + ":" + (minute < 10 ? "0" : "") + minute;
    }

    public int incrementHour(int diff){
        int dayDiff = 0;
        if (hour + diff < 0)
            dayDiff = -1;

        dayDiff += (hour + diff) / 24;
        hour = (hour + diff) % 24;
        if (hour < 0)
            hour += 24;
        return dayDiff;
    }

    public int incrementMinute(int diff){
        int hourDiff = 0;
        if (hour + diff < 0)
            hourDiff = -1;

        hourDiff += (minute + diff) / 60;
        minute = (minute + diff) % 60;
        if (minute < 0)
            minute += 60;
        return incrementHour(hourDiff);
    }

    public boolean isAfterHour(MyTime time2){
        return Integer.parseInt(toString().replaceAll(":", ""))
        > Integer.parseInt(time2.toString().replaceAll(":", ""));
    }

    public boolean isEqualHour(MyTime time2){
        return Integer.parseInt(toString().replaceAll(":", ""))
                == Integer.parseInt(time2.toString().replaceAll(":", ""));
    }

    public String timeDifference(MyTime time2){
        int minDiff;
        int hourDiff;
        if (!isAfterHour(time2)){
            minDiff = (60 - minute) + time2.minute;
            hourDiff = minDiff > 60 ? time2.hour - hour : time2.hour - hour - 1;
        }
        else if (isAfterHour(time2)){
            minDiff = (60 - time2.minute) + minute;
            hourDiff = minDiff > 60 ? hour - time2.hour : hour - time2.hour - 1;
        }
        else{
            minDiff = Math.abs(minute - time2.minute);
            hourDiff = 0;
        }

        if (hourDiff == 0 && minDiff == 0)
            return "";
        else if (hourDiff != 0 && minDiff == 0)
            return hourDiff + "hour(s)";
        else if (hourDiff == 0)
            return minDiff + "minute(s)";
        else
            return hourDiff + "hour(s) " + minDiff + "minute(s)";
    }
}
