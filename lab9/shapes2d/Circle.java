package shapes2d;

public class Circle {
    protected int radius;
    static final double pi = 3.14;

    public Circle(int radius){
        this.radius = radius;
    }

    public double area(){
        return radius * radius * pi;
    }

    public double perimeter(){
        return 2 * pi * radius;
    }

    public String toString(){
        return "radius = " + radius;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj instanceof Circle) {
            Circle c = (Circle) obj;
            return radius == c.radius;
        }
        return false;
    }
}
