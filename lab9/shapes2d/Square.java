package shapes2d;

public class Square {
    protected int side;

    public Square(int side){
        this.side = side;
    }

    public int area(){
        return side*side;
    }

    public String toString(){
        return "side = " + side;
    }

    public boolean equals(Object obj){
        if (this == obj)
            return true;

        if (this.getClass().equals(obj.getClass())){
            Square c = (Square) obj;
            return side == c.side;
        }

        return false;
    }
}