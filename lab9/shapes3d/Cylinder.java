package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    protected int height;

    public Cylinder(int radius, int height){
        super(radius);
        this.height = height;
    }

    public double area(){
        return 2 * super.area() + super.perimeter() * height;
    }

    public double volume(){
        return super.area() * height;
    }

    public String toString(){
        return super.toString() + ", height = " + height;
    }

    public boolean equals(Object obj){
        if (obj instanceof Cylinder) {
            Cylinder cy = (Cylinder) obj;

            return height == cy.height && radius == cy.radius;
        }
        return false;
    }
}
